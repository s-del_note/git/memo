# チートシート
```console
$ # ヘルプを表示
$ git help

$ # コマンドのオプションを調べる
$ git <COMMAND> -h
$ # 例
$ git add -h
$ git commit -h
```

<br>

## ローカルリポジトリ
```console
$ # カレントディレクトリを git の管理対象にする
$ git init

$ # 公開されているリモートリポジトリを複製してローカルリポジトリにする
$ git clone <URL>
```

<br>

## リモートリポジトリ
### リモート名
自由につけて良いが習慣的に `origin` や `upsteram` などが良く使われる
### 一覧の表示
```console
$ git remote -v
```
### 追加
```console
$ git remote add <REMOTE_NAME> <URL>
```
### URL 変更
```console
$ git remote set-url <REMOTE_NAME> <URL>
```

<br>

## ワーキングディレクトリ (ワークツリー)
### 現在の状況を確認
```console
$ git status

$ # 1 行の簡略化された表示
$ git status -s
```

<br>

## ブランチ
### 一覧の表示
```console
$ git branch

$ # -a オプションを指定するとリモート追跡ブランチも表示される
$ git branch -a

$ # ローカルブランチ一覧とそれの上流ブランチを表示
$ git branch -vv
```
### 作成
```console
$ git branch <BRANCH_NAME>

$ # 作成したブランチに切り替え
$ git switch -c <BRANCH_NAME>
```
### 名前の変更
```console
$ git branch -m <OLD_NAME> <NEW_NAME>
```
### 削除
```console
$ git branch -d <BRANCH_NAME>
```
### 切り替え
```console
$ git switch <BRANCH_NAME>
```
### 直前のブランチに切り替える (戻る)
```console
$ git switch -
```
### 取得したリモートブランチの内容を取り込んだローカルブランチに切り替え
1. `$ git fetch`
1. `$ git switch -c <LOCAL_BRANCH_NAME> <REMOTE_TRACKING_BRANCH_NAME>`
```console
$ # 例
$ git fetch
$ git switch -c develop origin/develop
```
### 上流ブランチを設定
```console
$ # ブランチ名を省略した場合は現在のブランチ名が指定される
$ git branch -u [BRANCH_NAME]
```

<br>

## 作業内容の取り消し
```console
$ # 全ての作業内容を取り消し
$ git restore .

$ # ファイルを指定して取り消し
$ git restore [FILENAMES]

$ # ディレクトリを指定して再帰的に取り消し
$ git resotre [DIRECTORY]
```

<br>

## 作業内容の無視
```console
$ git update-index --assume-unchanged [FILENAMES]

$ # どのファイルが無視状態か一覧表示
$ git ls-files -v
H README.md     # 無視状態でなければ大文字の H がファイル名先頭に表示される
h cheatsheet.md # 無視状態のファイルは小文字の h がファイル名先頭に表示される

$ # 無視設定を取り消すには --no-assume-unchanged を指定
$ git update-index --no-assume-unchanged [FILENAMES]
```

<br>

## 作業内容の退避
```console
$ git stash

$ # -u オプションで新規ファイルも含めて退避
$ git stash -u

$ # save でメッセージを付加
$ git stash save <MESSAGE>
```
### 一覧の表示
```console
$ git stash list
```
### 現在のブランチに退避した作業内容を戻す
```console
$ # スタッシュ一覧には残る
$ git stash apply <NUMBER>

$ # スタッシュ一覧に残らない
$ git stash pop <NUMBER>
```
### 退避した作業を消す
```console
$ # 作業内容を指定して消す
$ git stash drop <NUMBER>

$ # 全て消す
$ git stash clear
```

<br>

## ステージング (インデックスに追加)
```console
$ git add <PATH>

$ # -p オプションで変更内容を分割してステージング
$ git add -p <PATH>

$ # ステージングされている内容を確認
$ git diff --cached
```
### ステージから除外
```console
$ # すべてのファイルをステージから除外
$ git restore --staged .

$ # 特定のファイル
$ git restore --staged [FILES]
```

<br>

## コミット
```console
$ # エディタでメッセージを付加してコミット
$ git commit

$ # -m オプションでメッセージを付加
$ git commit -m <MESSAGE>
```
### 指定したコミットに戻す
```console
$ git restore -s <COMMIT_HASH>
```
### 一覧の表示
```console
$ # 過去のコミットログを表示
$ git log

$ # 1 行に簡略化されたログを表示
$ git log --oneline

$ # ファイルを指定して各行がどのコミットで最後に変更されたかを一覧で表示
$ git blame <FILEPATH>
```
### 複数のコミットの併合
```console
$ # コミットハッシュを指定して rebase
$ git rebase -i <HASH>

$ # HEAD から n 個遡った数で指定
$ # n を指定せず HEAD~ だと HEAD のひとつ前のコミットを指定
$ git rebase -i HEAD~n
```
上記のコマンドを使用すると `git-rebase-todo` という以下のようなファイルが開かれる  
```git-rebase-todo
pick cf67f1a [add] file
pick fc20a0d [add] 細かすぎるコミット1
pick 93aa07b [add] 細かすぎるコミット2
pick ed617e2 [add] 細かすぎるコミット3

# Commands:
# p, pick <commit> = use commit
# r, reword <commit> = use commit, but edit the commit message
# e, edit <commit> = use commit, but stop for amending
# s, squash <commit> = use commit, but meld into previous commit
# f, fixup <commit> = like "squash", but discard this commit's log message
# x, exec <command> = run command (the rest of the line) using shell
# b, break = stop here (continue rebase later with 'git rebase --continue')
# d, drop <commit> = remove commit
# l, label <label> = label current HEAD with a name
# t, reset <label> = reset HEAD to a label
# m, merge [-C <commit> | -c <commit>] <label> [# <oneline>]
# .       create a merge commit using the original merge commit's
# .       message (or the oneline, if no original merge commit was
# .       specified). Use -c <commit> to reword the commit message.
```
上のコミットの方が古く、下のコミットの方が新しい。  
まとめたいコミットの `pick` を `Commands` に記載されているコマンドに置き換える。  
コミットをまとめるなら基本的に `squash` か `fixup` を使用するが、
違いとしては既に掛かれているコミットメッセージを残すか残さないか、の違いがある。
```git-rebase-todo
pick cf67f1a [add] file
s fc20a0d [add] 細かすぎるコミット1
s 93aa07b [add] 細かすぎるコミット2
s ed617e2 [add] 細かすぎるコミット3
```
などとすると `pick cf67f1a [add] file` のコミットに、
`s` に置き換えた 3 つのコミットがまとめられる。  
保存して終了すると、エディタが開かれコミットメッセージを編集する状態になる。
### 直前のコミットメッセージの修正
```console
$ git commit --amend

$ # -m オプションでメッセージを編集
$ git commit --amend -m <MESSAGE>
```
### 2 つ以上前のコミットメッセージの修正
1. `$ git log` でログを確認して HEAD から修正したいコミットまでの数を数える
1. `$ git rebase -i HEAD~[n]`
1. 開かれたエディタ上で変更したいコミットの行を `pick` から `edit` に変更
1. 保存してエディタを終了
1. `$ git commit --amend` または `$ git commit --amend -m <MESSAGE>`
1. `$ git rebase --continue` (複数を `edit` にした場合は古い順に修正)

<br>

## マージ
```console
$ # 現在のブランチに指定したブランチを取り込む
$ # --no-ff を指定しないとマージコミットは作成されず fast-forward でのマージとなる
$ git merge --no-ff <BRANCH_NAME>
```
### 競合した場合の処理
1. 競合が発生したファイルをエディタで開く
1. 競合箇所が以下のように書き込まれている
    ```
    <<<<<<< HEAD
    aaa
    =======
    bbb
    >>>>>>> branch_b
    ```
1. 優先したい方だけを残して保存してエディタを閉じる
    ```
    bbb
    ```
1. 再度コミットする

<br>

## プッシュ
```console
$ # 現在のローカルブランチを上流ブランチにプッシュ
$ git push

$ # 上流ブランチを登録 (作成) してプッシュ
$ git push -u <REMOTE_NAME> <BRANCH_NAME>
```

<br>

## フェッチ・プル
```console
$ # 上流ブランチを更新
$ git fetch
$ # 指定したリモート追跡ブランチを更新
$ git fetch <REMOTE_NAME> <BRANCH_NAME>
$ # 全てのリモート追跡ブランチを更新
$ git fetch --all
$ git fetch <REMOTE_NAME>

$ # pull は fetch + merge を自動で行う
$ # 上流ブランチを更新してローカルブランチにマージ
$ git pull
$ # 指定したリモート追跡ブランチを更新してローカルブランチにマージ
$ git pull <REMOTE_NAME> <BRANCH_NAME>
$ # 全てのリモート追跡ブランチを更新してローカルブランチにマージ
$ git pull <REMOTE_NAME>
```

<br>

## 参考
- [【git stash】コミットはせずに変更を退避したいとき - Qiita](https://qiita.com/chihiro/items/f373873d5c2dfbd03250)
- [git add -p 使ってますか？ - Qiita](https://qiita.com/cotton_desu/items/bf08ac57d59b37dd5188)
- [Git で「追跡ブランチ」って言うのやめましょう - Qiita](https://qiita.com/uasi/items/69368c17c79e99aaddbf)

<br>

## `$ git help` 内容
```
$ git --version
git version 2.28.0.windows.1
$ git help
usage: git [--version] [--help] [-C <path>] [-c <name>=<value>]
           [--exec-path[=<path>]] [--html-path] [--man-path] [--info-path]
           [-p | --paginate | -P | --no-pager] [--no-replace-objects] [--bare]     
           [--git-dir=<path>] [--work-tree=<path>] [--namespace=<name>]
           <command> [<args>]

These are common Git commands used in various situations:

start a working area (see also: git help tutorial)
   clone             Clone a repository into a new directory
   init              Create an empty Git repository or reinitialize an existing one

work on the current change (see also: git help everyday)
   add               Add file contents to the index
   mv                Move or rename a file, a directory, or a symlink
   restore           Restore working tree files
   rm                Remove files from the working tree and from the index
   sparse-checkout   Initialize and modify the sparse-checkout

examine the history and state (see also: git help revisions)
   bisect            Use binary search to find the commit that introduced a bug
   diff              Show changes between commits, commit and working tree, etc
   grep              Print lines matching a pattern
   log               Show commit logs
   show              Show various types of objects
   status            Show the working tree status

grow, mark and tweak your common history
   branch            List, create, or delete branches
   commit            Record changes to the repository
   merge             Join two or more development histories together
   rebase            Reapply commits on top of another base tip
   reset             Reset current HEAD to the specified state
   switch            Switch branches
   tag               Create, list, delete or verify a tag object signed with GPG

collaborate (see also: git help workflows)
   fetch             Download objects and refs from another repository
   pull              Fetch from and integrate with another repository or a local branch
   push              Update remote refs along with associated objects

'git help -a' and 'git help -g' list available subcommands and some
concept guides. See 'git help <command>' or 'git help <concept>'
to read about a specific subcommand or concept.
See 'git help git' for an overview of the system.
```
