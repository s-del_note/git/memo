# Git メモ


## .gitconfig
git の設定ファイル。以下の 3 種類が存在する。
1. `system`
1. `global`
1. `local`

上記の順番で読み込まれ、後から読み込まれた設定で上書きされる。
### system
git インストールディレクトリ内に存在
### global
ホームディレクトリ内に存在
- Linux: `/home/<USERNAME>/.gitconfig`
- Windows: `C:\Users\<USERNAME>\.gitconfig`
### local
対象リポジトリの `.git` 内に存在
### 設定一覧を出力
```console
$ git config --list
$ git config --list --system
$ git config --list --global
$ git config --list --local
```

<br>

## SSH 秘密鍵を .gitconfig に登録
```console
$ git config --global core.sshCommand "ssh -i <SSH_KEYPATH>"
```

<br>

## デフォルトブランチ名の変更
```console
$ # 永続的な変更
$ git config --global init.defaultBranch main

$ # init 時に指定
$ git init --initial-branch main
```

<br>

## merge と pull でのファストフォワード (ff) の設定
```console
$ # マージ時は no-ff
$ git config --global merge.ff false

$ # プル時は ff でマージ
$ git config --global pull.ff only
```

<br>

## `git status` コマンドでのファイル名の文字化けを解消
```console
$ git config --global core.quotepath false
```

<br>

## コミット時のメッセージ編集用エディタの指定
```console
$ git config --global core.editor "code --wait"
```

<br>

## ツリー形式の log を見れるようにエイリアスを登録する
```conf
# tree という名前で以下の log とそのオプションを登録
[alias]
	tree = log --graph --all --pretty=format:'%x09%C(auto)%h %Cgreen%ar %x09%C(cyan ul)%an%Creset %x09%C(auto)%s %d'
```
```console
$ # エイリアスを実行
$ git tree
```

<br>

## リポジトリ URL の形式
- http: `https://<DOMAIN>/<USER_NAME>/<REPOSITORY_NAME>.git`
- ssh: `git@<DOMAIN>:<USER_NAME>/<REPOSITORY_NAME>.git`

<br>

## 参考
- [gitconfig の基本を理解する - Qiita](https://qiita.com/shionit/items/fb4a1a30538f8d335b35)
- [git clone 時に秘密鍵を指定する - Qiita](https://qiita.com/sonots/items/826b90b085f294f93acf)
- [Gitのデフォルト・ブランチ名を変更する方法  |  Rriver](https://parashuto.com/rriver/tools/change-git-default-branch-name)
- [gitのmerge --no-ff のススメ - Qiita](https://qiita.com/nog/items/c79469afbf3e632f10a1)
- [git diff や git status での日本語の文字化けを防ぐ (core.page, core.quotepath) - まくまくGitノート](https://maku77.github.io/git/settings/garbling.html)
- [git logのオプションと綺麗にツリー表示するためのエイリアス - Qiita](https://qiita.com/kawasaki_dev/items/41afaafe477b877b5b73)
